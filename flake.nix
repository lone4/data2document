{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    devshell.url = "github:numtide/devshell";
  };

  outputs = inputs@{ flake-parts, nixpkgs, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.devshell.flakeModule
      ];
      systems = nixpkgs.lib.systems.flakeExposed;

      perSystem = { config, self', inputs', pkgs, system, ... }: {
        # Per-system attributes can be defined here. The self' and inputs'
        # module parameters provide easy access to attributes of the same
        # system.

        # Equivalent to  inputs'.nixpkgs.legacyPackages.hello;
        packages.default = pkgs.pandoc;

        devshells = {
          default = {
            commands = [
              { name = "md2epub"; help = "generate .html and .epub file";
                command = ''
                  pandoc "$1" \
                    -f gfm \
                    --toc \
                    --css epub.css \
                    --metadata title="Tttle" \
                    --metadata author="authorrr" \
                    --metadata subject="templat0r" \
                    --metadata keywords="pandoc,xelatex,python,template,jinja2" \
                    --highlight-style pygments.theme \
                    -V toc-title='Table of contents' \
                    -V linkcolor:blue \
                    -V geometry:a4paper \
                    -V geometry:margin=2cm \
                    -V mainfont="DejaVu Serif" \
                    -V monofont="DejaVu Sans Mono" \
                  -o "$2"
                '';
              }
              { name = "md2pdf"; help = "generate .pdf";
                command = ''
                  pandoc "$1" \
                    -f gfm \
                    --toc \
                    --include-before-body cover.tex \
                    --include-in-header pdf_properties.tex \
                    --include-in-header chapter_break.tex \
                    --include-in-header inline_code.tex \
                    --include-in-header bullet_style.tex \
                    --include-in-header quote.tex \
                    --metadata title="Tttle" \
                    --metadata author="authorrr" \
                    --metadata subject="templat0r" \
                    --metadata keywords="pandoc,xelatex,python,template,jinja2" \
                    --highlight-style pygments.theme \
                    -V toc-title='Table of contents' \
                    -V linkcolor:blue \
                    -V geometry:a4paper \
                    -V geometry:margin=2cm \
                    -V mainfont="DejaVu Serif" \
                    -V monofont="DejaVu Sans Mono" \
                    --pdf-engine=xelatex \
                  -o "$2"
                '';
              }
            ];

            packages = with pkgs; [
              texliveFull
              pandoc
              pandoc-acro
              pandoc-include
              pandoc-tablenos
              pandoc-fignos
              pandoc-secnos
              librsvg
              #haskellPackages.pandoc-crossref
              #haskellPackages.pandoc-columns
              (python3.withPackages(ps: with ps; [
                jinja2
                pydantic
              ]))
            ];
          };

          notebook = {
            env = [
              { name = "HTTP_PORT"; value = 8080; }
              { name = "NB_DIR"; value = "./datawidgets"; }
              { name = "NB_NAME"; value = "./datawidgets/datawidget.ipynb"; }
            ];

            commands = [
              {
                help = "start notebook server on localhost";
                name = "stnb";
                command = "jupyter notebook --ServerApp.root_dir=$NB_DIR --port $HTTP_PORT";
              }
              {
                help = "start widget notebook server on localhost";
                name = "widget";
                command = "jupyter notebook --ServerApp.root_dir=$NB_DIR --port $HTTP_PORT $NB_NAME";
              }
            ];

            packages = with pkgs; [
              (python3.withPackages(ps: with ps; [
                jupyter
                ipython
                ipywidgets
                #vega
                scikit-learn
                pywavelets

                requests
                jinja2

                numpy
                pandas
                matplotlib
              ]))
            ];
          };
        };
      };
  };
}
